CFLAGS += -Wall -W
CPPFLAGS += -I.
YFLAGS = -d

SRCS  = dump_cis.c pack_cis.c
HDRS  = pack_cis.h
TOOLS = dump_cis pack_cis

all:	$(SRCS) $(HDRS) $(TOOLS)

dump_cis: dump_cis.o cistpl.o

yacc_cis.o: yacc_cis.c
	$(CC) -MD $(CFLAGS) $(CPPFLAGS) -c $<
	@mkdir -p .depfiles ; mv $*.d .depfiles

lex_cis.o: lex_cis.c yacc_cis.h
	$(CC) -MD $(CFLAGS) -Wno-unused-function $(CPPFLAGS) -c $<
	@mkdir -p .depfiles ; mv $*.d .depfiles

pack_cis: pack_cis.o lex_cis.o yacc_cis.o
	$(CC) $+ -o $@ -lm

parser: lex_cis.o yacc_cis.c
	$(CC) $(CFLAGS) $(CPPFLAGS) -DDEBUG -o $@ $+

clean:
	rm -f core core.* *.o *.s *.a *~ .depend .depfiles/*.d
	rm -f $(TOOLS) lex_cis.c yacc_cis.c yacc_cis.h

install: $(TOOLS)
	@mkdir -p $(PREFIX)/sbin
	cp -f dump_cis pack_cis $(PREFIX)/sbin

%.c %.h : %.y
	$(YACC) $(YFLAGS) $<
	mv y.tab.c $*.c
	mv y.tab.h $*.h

%.s : %.c
	$(CC) $(CFLAGS) $(CPPFLAGS) -S $<

# Stuff to automatically maintain dependency files

%.o : %.c
	$(CC) -MD $(CFLAGS) $(CPPFLAGS) -c $<
	@mkdir -p .depfiles ; mv $*.d .depfiles

-include $(SRCS:%.c=.depfiles/%.d)
